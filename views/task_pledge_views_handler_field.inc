<?php

/**
 * @file
 *
 * A Views' field handler for task_pledge.
 *
 */
class task_pledge_views_handler_field extends views_handler_field {

  // These constants are used in the database and also in the radio buttons.
  // SKIP is not actually used in the database, because a missing record
  // implies SKIP.
  const SKIP = 'skip';
  const PLEDGED = 'pledged';
  const COMPLETED = 'completed';

  // Same as in task_pledge.install.
  const TASK_PLEDGE_TABLE = 'task_pledge';

  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
    $this->additional_fields['title'] = 'title';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render the field contents.
   *
   * @param $values
   * @return string
   */
  function render($values) {
    // Render a Views form item placeholder.
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * Add task pledge/completed radio buttons to the form.
   */
  function views_form(&$form, &$form_state) {
    // Create a container for our replacements
    $form[$this->options['id']] = array(
      '#type' => 'container',
      '#tree' => TRUE,
    );

    // Get skip/pledged/completed for all the tasks.
    $nid_to_status = $this->fetch_nid_to_status_for_current_user();

    // Get counts of how many users pledged or completed each task.
    $nid_to_pledged_count = $this->fetch_status_counts(self::PLEDGED);
    $nid_to_completed_count = $this->fetch_status_counts(self::COMPLETED);

    // Iterate over the result and add our replacement fields to the form.
    foreach($this->view->result as $row_index => $row) {
      // Get the nid value for this row.
      $nid = $row->{$this->aliases['nid']};

      // Get counts
      $current_status =  get($nid_to_status, $nid, self::SKIP);
      $others_pledged = get($nid_to_pledged_count, $nid, 0) -
	($current_status == self::PLEDGED ? 1 : 0);
      $others_completed = get($nid_to_completed_count, $nid, 0) -
	($current_status == self::COMPLETED ? 1 : 0);

      // Add radio buttons to the form.  This array convention
      // corresponds to the placeholder HTML comment syntax.
      $form[$this->options['id']][$row_index] = array(
        '#type' => 'radios',
        '#default_value' => $current_status,
        '#options' => array(
			    self::SKIP => t('Skip'),
			    self::PLEDGED => t('Pledged') .
			    ' (+' . format_plural($others_pledged, "1 other)", "@count others)"),
			    self::COMPLETED => t('Completed') .
			    ' (+' . format_plural($others_completed, "1 other)", "@count others)"),
			    ),
	'#element_validate' => array('task_pledge_views_handler_field_validate'),
        '#required' => TRUE,
      );
    }

    // Submit to the current page if not a page display.
    if ($this->view->display_handler->plugin_name != 'page') {
      $form['#action'] = current_path();
    }
  }

  /**
   * Form submit method.
   */
  function views_form_submit($form, &$form_state) {
    // Determine which nodes we need to update.
    $updates = array();
    foreach($this->view->result as $row_index => $row) {
      // dpm($row, 'row');
      $row_nid = $row->{$this->aliases['nid']};
      $status = $form_state['values'][$this->options['id']][$row_index];
      $this->update_status($row_nid, $status);
      // dpm($row_nid, 'nid');
      // dpm($choice_value, 'choice');

    }

    drupal_set_message(t('Updated your pledges and completions.'));
  }

  /**
   * Look up all the status values for all the nids for the current user.
   */
  private function fetch_nid_to_status_for_current_user() {
    global $user;
    $uid = $user->uid;
    $statuses = array();
    if ($uid == 0) {
      // Do nothing for non-logged in users.
      return $statuses;
    }

    return db_query("SELECT nid, status from {task_pledge} where uid=:uid",
		    array(':uid' => $uid))->fetchAllKeyed();
  }

  /**
   * Count how many users have pledged or completed (use $status) each task.
   * @return 
   */
  private function fetch_status_counts($status) {
    return
      db_query("SELECT nid, count(*) from {task_pledge} WHERE status=:status GROUP BY nid",
	       array(':status' => $status))->fetchAllKeyed();
  }
				     
  /**
   * Update the status field in the task_pledge table for the given task nid.
   */
  private function update_status($nid, $status) {
    global $user;
    $uid = $user->uid;
    if ($uid == 0) {
      // Do nothing for non-logged in users.
      return;
    }

    switch($status) {
    case self::SKIP:
      // SKIP is not stored.
      db_delete(self::TASK_PLEDGE_TABLE)
	->condition(db_and()->condition('nid', $nid)->condition('uid', $uid))
	->execute();
      break;

    case self::PLEDGED:
    case self::COMPLETED:
      // PLEDGED and COMPLETED are stored as their respective strings.
      db_merge(self::TASK_PLEDGE_TABLE)
	->key(array('nid' => $nid, 'uid' => $uid))
	->fields(array('status' => $status))
	->execute();
      break;
    }
  }

}

/**
 * Validation callback for the title element.
 *
 * @param $element
 * @param $form_state
 */
function task_pledge_views_handler_field_validate($element, &$form_state) {
  if (!in_array($element['#value'],
		array(task_pledge_views_handler_field::SKIP,
		      task_pledge_views_handler_field::PLEDGED,
		      task_pledge_views_handler_field::COMPLETED))) {
    form_error($element, t('Pledge field has bad value: ' . $element['#value']));
  }
}

/**
 * Get from an array with a default value if not found.
 */
function get($ary, $key, $default) {
  return isset($ary[$key]) ? $ary[$key] : $default;
}

/**
 * Return singular or plural for persons.
 */
function persons($n) {
  return $n == 1 ? 'person' : 'people';
}



