<?php
/**
 * @file
 * Provide a pledge status field to add to views.
 */

/**
 * Implements hook_views_data_alter().
 *
 * @param $data
 *   Information about Views' tables and fields.
 */
function task_pledge_views_data_alter(&$data) {
  // Add the task pledge form field to the node-related fields.
  $data['node']['task_pledge'] = array(
    'field' => array(
      'title' => t('Task Pledge field'),
      'help' => t('Choose to pledge or complete a task'),
      'handler' => 'task_pledge_views_handler_field',
    ),
  );
}

/**
 * Implements hook_views_pre_render().
 */

function task_pledge_views_pre_render(&$view) {
  // Only modify views that end in 'tasks' and that have a 'points' field.
  if( !(task_pledge_ends_with($view->name, 'tasks') &&
	isset($view->result[0]->field_field_points))) {
    // Not a view we need to modify.
    return;
  }

  // Fetch all the points values for all the tasks, given a sample task nid.
  $task_nids_to_points = get_task_nids_to_points($view->result[0]->nid);

  // Fetch all the pledged nids.
  $all_pledged_nids =
    db_query("SELECT nid from {task_pledge} WHERE status=:status",
	     array(':status' => task_pledge_views_handler_field::PLEDGED))->fetchCol();
  // Sum points.
  $all_pledged_nids_sum = points_sum_for_nids($all_pledged_nids, $task_nids_to_points);

  // Fetch all the completed nids.
  $all_completed_nids =
    db_query("SELECT nid from {task_pledge} WHERE status=:status",
	     array(':status' => task_pledge_views_handler_field::COMPLETED))->fetchCol();
  // Sum points.
  $all_completed_nids_sum = points_sum_for_nids($all_completed_nids, $task_nids_to_points);

  // Fetch all the users with pledges and/or completions.
  $uids_to_users =
    user_load_multiple(db_query("SELECT DISTINCT uid from {task_pledge}")->fetchCol());

  $usernames = array();
  foreach ($uids_to_users as $one_user) {
    $usernames[] = $one_user->name;
  }
  sort($usernames);

  // Get this user's uid.
  global $user;
  $uid = $user->uid;

  // Fetch this user's pledged nids.
  $user_pledged_nids =
    db_query("SELECT nid from {task_pledge} WHERE status=:status AND uid=:uid",
	     array(':status' => task_pledge_views_handler_field::PLEDGED,
		   ':uid' => $uid))->fetchCol();
  // Sum points.
  $user_pledged_nids_sum = points_sum_for_nids($user_pledged_nids, $task_nids_to_points);

  // Fetch this user's completed nids.
  $user_completed_nids =
    db_query("SELECT nid from {task_pledge} WHERE status=:status AND uid=:uid",
	     array(':status' => task_pledge_views_handler_field::COMPLETED,
		   ':uid' => $uid))->fetchCol();
  // Sum points.
  $user_completed_nids_sum = points_sum_for_nids($user_completed_nids, $task_nids_to_points);

  // Now make a table for the sums.
  $header = array(t('Totals'), t('Points Pledged but not yet Completed'), t('Points Completed'));
  $rows = array();
  $rows[] = array($user->name, $user_pledged_nids_sum, $user_completed_nids_sum);
  $rows[] = array(t('Everyone'), $all_pledged_nids_sum, $all_completed_nids_sum);
  $table = array(
		 '#theme' => 'table',
		 '#header' => $header,
		 '#rows' => $rows,
		 );
  $view->attachment_before =
    "<h2>Participants</h2>\n" .
    "<p>" . implode(', ', $usernames) . "</p>\n" .
    "<h2>Tasks - Point Totals</h2>\n" .
    drupal_render($table) .
    "<h2>Tasks - Your Pledges and Completions</h2>\n";
}

/**
 * Given an array of nids, and the nids_to_points array,
 * return the sum of the points for the given nids.
 */
function points_sum_for_nids($nids, $nids_to_points) {
  $sum = 0;
  foreach ($nids as $nid) {
    // Skip missing nids.
    if (isset($nids_to_points[$nid])) {
      $sum += $nids_to_points[$nid];
    }
  }
  return $sum;
}

/**
 * Return a map of task nids to their point values.
 * Pass in a sample nid of the correct content type.
 */
function get_task_nids_to_points($nid) {
  // Get one node, corresponding to the $nid passed in.
  $sample_node = node_load($nid);
  $task_content_type = $sample_node->type;

  // Get all nodes of the same content type.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $task_content_type)
    ->propertyCondition('status', 1);             // published

  // The results are "stub" entities, with just nids and vids (version ids).
  $results = $query->execute();
  // Get the node stubs.
  $tasks = $results['node'];
  // Find out how to identify field_points.
  $fields = field_info_instances('node', $task_content_type);
  $points_field_id = $fields['field_points']['field_id'];
  field_attach_load('node', $tasks, FIELD_LOAD_CURRENT, array('field_id' => $points_field_id));
  // Return a map of task nids to their point values.
  $nid_to_points = array();
  foreach ($tasks as $task) {
    // See https://www.drupal.org/node/1052514#comment-8227775 for why we use LANGUAGE_NONE.
    // The value is a string, so cast it to an int.
    $nid_to_points[$task->nid] = (int) $task->field_points[LANGUAGE_NONE][0]['value'];

  }
  return $nid_to_points;
}

/**
 * True if $haystack ends with $needle.
 */
function task_pledge_ends_with($haystack, $needle)
{
    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}